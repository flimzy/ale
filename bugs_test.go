package ale_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/flimzy/ale"
	"gitlab.com/flimzy/testy"
)

func TestPanicNotConvertedTo500(t *testing.T) {
	var logCtx struct {
		Status int
		Error  string
	}

	middlewares := ale.New()
	middlewares.SetLogger(func(c *ale.LogContext) {
		logCtx.Status = c.StatusCode
		if c.Error != nil {
			logCtx.Error = c.Error.Error()
		}
	})
	middlewares.Use(ale.Recover())

	h := http.HandlerFunc(func(http.ResponseWriter, *http.Request) {
		panic("oh noes!")
	})

	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/", nil)
	middlewares.Handler(h).ServeHTTP(rec, req)

	res := rec.Result()
	defer res.Body.Close() //nolint:errcheck // don't care

	if d := testy.DiffHTTPResponse(testy.Snapshot(t), res); d != nil {
		t.Error(d)
	}
	if d := testy.DiffInterface(testy.Snapshot(t, "cx"), logCtx); d != nil {
		t.Errorf("Unexpected log context: %s", d)
	}
}
