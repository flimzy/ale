package ale

import (
	"encoding/json"
	"net/http"
)

// ServeJSON marshals i and serves it on w. If the Content-Type header is not
// yet set on w, it will be set to "application/json; charset=utf-8". If status
// is passed, the first status value will be passed to w.WriteHeader, otherwise
// the default of http.StatusOK is used.
func ServeJSON(w http.ResponseWriter, i interface{}, status ...int) error {
	if w.Header().Get("Content-Type") == "" {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
	}
	if len(status) > 0 {
		w.WriteHeader(status[0])
	}
	return json.NewEncoder(w).Encode(i)
}
