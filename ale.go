package ale

import (
	"errors"
	"net/http"

	"gitlab.com/flimzy/ale/httperr"
)

type contextKey struct{ name string }

// Handler extends http.Handler with an error return value.
type Handler interface {
	// ServeHTTPE serves the same purpose as http.Handler's ServeHTTP, except
	// that it may return an error. The function should _either_ write to
	// http.ResponseWriter _or_ return an error.
	ServeHTTPE(http.ResponseWriter, *http.Request) error
}

// HandlerFunc extends the standard http.HandlerFunc to support error
// handling more easily.
//
// HandlerFunc also satisfies the http.Handler interface by implementing the
// ServeHTTP() method, which will panic on error, such that the panic can be
// recovered properly by the Recover() middleware or Chain.Handler().
type HandlerFunc func(http.ResponseWriter, *http.Request) error

// panicErr annotates an error for proper recovery by the Recover() middleware.
type panicErr struct{ error }

// ServeHTTP calls f, and if it returns an error, it is panicked such that it
// can be properly recovered by the Recover() middleware or Chain.Handler().
func (f HandlerFunc) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := f.ServeHTTPE(w, r); err != nil {
		panic(panicErr{err})
	}
}

func (f HandlerFunc) ServeHTTPE(w http.ResponseWriter, r *http.Request) error {
	return f(w, r)
}

// Wrap wraps a standard http.Handler to satisfy the Handler interface.
func Wrap(h http.Handler) Handler {
	return WrapFunc(h.ServeHTTP)
}

// WrapFunc wraps a standard http.HandlerFunc to satisfy the Handler interface.
type WrapFunc func(http.ResponseWriter, *http.Request)

func (f WrapFunc) ServeHTTPE(w http.ResponseWriter, r *http.Request) (err error) {
	defer func() {
		r := recover()
		if r == nil {
			return
		}
		if pe, ok := r.(panicErr); ok {
			err = pe.error
			return
		}
		panic(r)
	}()
	f(w, r)
	return nil
}

// Middleware extends the standard middleware with error handling.
type Middleware func(Handler) Handler

// ErrorReporter is a function that can report an HTTP error returned by a
// ErrorHandlerFunc.
type ErrorReporter func(w http.ResponseWriter, r *http.Request, err error)

type reportedError struct{ error }

func (e reportedError) Unwrap() error { return e.error }

// Middleware converts c into a middleware function.
func (c ErrorReporter) Middleware(next Handler) Handler {
	return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		err := next.ServeHTTPE(w, r)
		var reported reportedError
		if errors.As(err, &reported) {
			return err
		}
		if err != nil {
			c(w, r, err)
			err = reportedError{err}
		}
		return err
	})
}

// HTTPHandlerFunc wraps a HandlerFunc, such that it can be used as a standard
// http.HandlerFunc.
//
// Deprecated: Use Middleware() method.
func (c ErrorReporter) HTTPHandlerFunc(f HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := f(w, r); err != nil {
			c(w, r, err)
		}
	})
}

// Std converts h into a standard http.Handler.  If h returns an error, it is
// panicked such that it can be recovered by the Recover() middleware or
// Chain.Handler().
func Std(h Handler) http.Handler {
	if hf, ok := h.(http.Handler); ok {
		return hf
	}
	return HandlerFunc(h.ServeHTTPE)
}

// ConvertMiddleware converts ale middleware to standard middleware. See Std()
// to understand how errors are handled.
func ConvertMiddleware(emw Middleware) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return Std(emw(Wrap(next)))
	}
}

// StdMiddleware adapts a standard middleware function for use with ale.
func StdMiddleware(mw func(http.Handler) http.Handler) Middleware {
	return func(next Handler) Handler {
		return Wrap(mw(Std(next)))
	}
}

// middlewares is a chain of ale middlewares.
type middlewares []Middleware

// Use adds mw to the end of the middleware chain.
func (m *middlewares) Use(mw ...Middleware) {
	*m = append(*m, mw...)
}

// UseStd adds mw to the end of the middleware chain.
func (m *middlewares) UseStd(mws ...func(http.Handler) http.Handler) {
	for _, mw := range mws {
		*m = append(*m, StdMiddleware(mw))
	}
}

// DefaultReporter is used by Std() any time an error has not been injected
// with HandleErrors.
var DefaultReporter = BasicReporter

// BasicReporter is a bare ErrorReporter, which wraps http.Error.  If err
// satisfies the interface{ HTTPStatus() int } interface, the method is called
// to determine the HTTP status.
//
// If w is a UsedWriter, and returns true, then nothing is written to the
// response.
func BasicReporter(w http.ResponseWriter, r *http.Request, err error) {
	if dw, ok := w.(UsedWriter); ok {
		if dw.Used() {
			return
		}
	}
	status := httperr.HTTPStatus(err)
	http.Error(w, err.Error(), status)
}

// Chain represents a complete middleware chain and adapter.
type Chain struct {
	reporter ErrorReporter
	logger   func(*LogContext)
	middlewares
}

// New returns an empty Ale middleware chain.
func New() *Chain {
	return &Chain{
		reporter: BasicReporter,
	}
}

// SetErrerReporter sets the ErrorReporter, to handle any errors generated by
// a request. If unset, BasicReporter is used.
func (c *Chain) SetErrorReporter(r ErrorReporter) {
	c.reporter = r
}

// SetLogger sets a request logger. If unset, requests are not logged by ale.
func (c *Chain) SetLogger(l func(*LogContext)) {
	c.logger = l
}

// Handler applies the middleware chain to h.
func (c *Chain) Handler(h http.Handler) http.Handler {
	var next Handler = Wrap(h)
	for i := len(c.middlewares) - 1; i >= 0; i-- {
		next = c.middlewares[i](next)
	}

	next = c.reporter.Middleware(next)

	if c.logger != nil {
		next = RequestLogger(c.logger)(next)
	}

	next = TrackWriterUsage(next)

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := next.ServeHTTPE(w, r)
		repErr := reportedError{}
		if errors.As(err, &repErr) {
			err = nil
		}
		if err != nil {
			panic("ale bug: error escaped middleware stack: " + err.Error())
		}
	})
}
