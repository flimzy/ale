package panicerr

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/flimzy/ale/httperr"
)

func TestRecover(t *testing.T) {
	type tst struct {
		fn     func() error
		status int
		err    string
		stack  string
	}
	tests := testy.NewTable()
	tests.Add("No panic, no error", tst{
		fn: func() error { return nil },
	})
	tests.Add("No panic, with error", tst{
		fn:     func() error { return httperr.New(400, "foo") },
		status: 400,
		err:    "foo",
	})
	tests.Add("string panic", tst{
		fn:     func() error { panic("oink") },
		status: 500,
		err:    "oink",
		stack:  "gitlab.com/flimzy/ale/panicerr.TestRecover.func3",
	})
	tests.Add("error panic", tst{
		fn:     func() error { panic(httperr.New(404, "not found")) },
		status: 404,
		err:    "not found",
		stack:  "gitlab.com/flimzy/ale/panicerr.TestRecover.func4",
	})
	tests.Add("int panic", tst{
		fn:     func() error { panic(int(123)) },
		status: 500,
		err:    "123",
		stack:  "gitlab.com/flimzy/ale/panicerr.TestRecover.func5",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		err := func() (err error) {
			defer Recover(&err)
			return test.fn()
		}()
		t.Run("errCheck", func(t *testing.T) {
			testy.StatusError(t, test.err, test.status, err)
		})
		t.Run("stackCheck", func(t *testing.T) {
			if test.stack == "" {
				t.Skip("No stack defined in test")
			}
			stacker, ok := err.(stackTracer)
			if !ok {
				t.Fatal("Expected a stackTracer")
			}
			stack := fmt.Sprintf("%+v", stacker.StackTrace())
			parts := strings.Split(stack, "\n")
			if test.stack != parts[1] {
				t.Errorf("First line of stack trace does not match %q:\n%s", test.stack, stack)
			}
		})
	})
}
