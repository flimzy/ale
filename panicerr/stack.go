package panicerr

import (
	"fmt"
	"runtime"

	"github.com/pkg/errors"
)

type (
	// Frame is an alias to pkg/errors.Frame
	Frame = errors.Frame
	// StackTrace is an alias to pkg/errors.StackTrace
	StackTrace = errors.StackTrace
)

type stackTracer interface {
	StackTrace() StackTrace
}

// stack represents a stack of program counters.
type stack []uintptr

func (s *stack) Format(st fmt.State, verb rune) {
	switch verb {
	case 'v':
		switch {
		case st.Flag('+'):
			for _, pc := range *s {
				f := Frame(pc)
				fmt.Fprintf(st, "\n%+v", f)
			}
		}
	}
}

func (s *stack) StackTrace() StackTrace {
	f := make([]Frame, len(*s))
	for i := 0; i < len(f); i++ {
		f[i] = Frame((*s)[i])
	}
	return f
}

func callers() *stack {
	const depth = 32
	var pcs [depth]uintptr
	n := runtime.Callers(skipFrames, pcs[:])
	var st stack = pcs[0:n]
	return &st
}
