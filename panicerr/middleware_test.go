package panicerr

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/go-chi/chi/v5"
	"gitlab.com/flimzy/testy"

	"gitlab.com/flimzy/ale"
)

func TestRecoverer(t *testing.T) {
	type tst struct {
		handler http.HandlerFunc
		status  int
		err     string
		stack   string
	}
	tests := testy.NewTable()
	tests.Add("No panic", tst{
		handler: func(w http.ResponseWriter, _ *http.Request) {
			w.WriteHeader(200)
		},
	})
	tests.Add("panic", tst{
		handler: func(w http.ResponseWriter, _ *http.Request) {
			panic("oink")
		},
		status: http.StatusInternalServerError,
		err:    "oink",
		stack:  "gitlab.com/flimzy/ale/panicerr.TestRecoverer.func2",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		r := chi.NewMux()
		var err error
		r.Use(Recoverer(ale.ErrorReporter(func(_ http.ResponseWriter, _ *http.Request, e error) {
			err = e
		})))
		r.Handle("/", test.handler)
		w := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		r.ServeHTTP(w, req)

		t.Run("errCheck", func(t *testing.T) {
			testy.StatusError(t, test.err, test.status, err)
		})
		t.Run("stackCheck", func(t *testing.T) {
			if test.stack == "" {
				t.Skip("No stack defined in test")
			}
			stacker, ok := err.(stackTracer)
			if !ok {
				t.Fatal("Expected a stackTracer")
			}
			stack := fmt.Sprintf("%+v", stacker.StackTrace())
			parts := strings.Split(stack, "\n")
			if test.stack != parts[1] {
				t.Errorf("First line of stack trace does not match %q:\n%s", test.stack, stack)
			}
		})
	})
}
