package panicerr

import (
	"net/http"

	"gitlab.com/flimzy/ale"
)

// Recoverer returns an HTTP middleware that catches panics, and sends them
// to reporter.
func Recoverer(reporter ale.ErrorReporter) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := func() (err error) {
				defer Recover(&err)
				next.ServeHTTP(w, r)
				return nil
			}()
			if err != nil {
				reporter(w, r, err)
			}
		})
	}
}
