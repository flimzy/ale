[![Go Report Card](https://goreportcard.com/badge/gitlab.com/flimzy/ale)](https://goreportcard.com/report/gitlab.com/flimzy/ale)
[![GoDoc](https://godoc.org/gitlab.com/flimzy/ale?status.svg)](https://pkg.go.dev/gitlab.com/flimzy/ale)

![Ale logo](artwork/ale-logo.png)


Ale
---

Ale is a collection of HTTP-related tools to be used with roll-your-own Go
web applications.

This software is released under the MIT license, as outlined in the included
LICENSE.md file.
