package ale

import (
	stderr "errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/flimzy/ale/errors"
)

func TestRecover(t *testing.T) {
	handler := HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		panic("failure!")
	})

	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/", nil)
	err := Recover()(handler).ServeHTTPE(rec, req)

	if !testy.ErrorMatches("panic recovered: failure!", err) {
		t.Errorf("Unexpected error: %s", err)
	}
	const want = "recover_test.go:17"
	stack := errors.InspectStackTrace(err)
	if got := fmt.Sprintf("%v", stack[0]); got != want {
		t.Errorf("unexpected stack frame: %s", got)
	}
}

func TestPanicToerror(t *testing.T) {
	type tt struct {
		panic interface{}
		err   string
		stack string
	}

	tests := testy.NewTable()
	tests.Add("nil", tt{
		panic: nil,
	})
	tests.Add("string", tt{
		panic: "panic string",
		err:   "panic recovered: panic string",
		stack: "recover_test.go:62",
	})
	tests.Add("standard error", tt{
		panic: stderr.New("stdlib err"),
		err:   "panic recovered: stdlib err",
		stack: "recover_test.go:62",
	})
	tests.Add("error with stack", tt{
		panic: errors.New("with stack"),
		err:   "panic recovered: with stack",
		stack: "recover_test.go:56",
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		err := PanicToError(tt.panic)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if err == nil {
			return
		}
		stack := errors.InspectStackTrace(err)
		if got := fmt.Sprintf("%v", stack[0]); got != tt.stack {
			t.Errorf("unexpected stack frame: %s", got)
		}
	})
}
