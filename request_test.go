package ale

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/flimzy/testy"
)

func TestNewLogContext(t *testing.T) {
	now = func() time.Time {
		return time.Time{}
	}
	defer func() { now = time.Now }()
	w := struct{ http.ResponseWriter }{}
	r := httptest.NewRequest("GET", "/", nil)
	r.RemoteAddr = "192.168.0.1"
	result := NewLogContext(w, r)

	expected := &LogContext{
		w:         w,
		Request:   r,
		StartTime: time.Time{},
		Data:      map[string]interface{}{},
	}
	if d := testy.DiffInterface(expected, result); d != nil {
		t.Error(d)
	}
}

func TestSetResponseHeaders(t *testing.T) {
	w := httptest.NewRecorder()
	w.Header().Set("Trailer", "X-Foo")
	w.Header().Set("X-Foo", "Moo")
	w.Header().Set("X-Baz", "Oink")
	w.Header().Set("Trailer:X-Qux", "Quack")
	l := &LogContext{w: w}
	t.Run("setResponseHeader", func(t *testing.T) {
		l.setResponseHeader()
		expected := http.Header{
			"X-Baz": []string{"Oink"},
		}
		if d := testy.DiffInterface(expected, l.ResponseHeader); d != nil {
			t.Error(d)
		}
	})
	t.Run("setResponseTrailer", func(t *testing.T) {
		l.setResponseTrailer()
		expected := http.Header{
			"X-Foo": []string{"Moo"},
			"X-Qux": []string{"Quack"},
		}
		if d := testy.DiffInterface(expected, l.ResponseTrailer); d != nil {
			t.Error(d)
		}
	})
}
