package ale

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/flimzy/ale/httperr"
)

func Test_Handler_ServeHTTP(t *testing.T) {
	type tt struct {
		ctx     context.Context
		handler HandlerFunc
	}

	tests := testy.NewTable()
	tests.Add("standard error", tt{
		handler: func(http.ResponseWriter, *http.Request) error {
			return errors.New("foo")
		},
	})
	tests.Add("no error", tt{
		handler: func(http.ResponseWriter, *http.Request) error {
			return nil
		},
	})
	tests.Add("httperr", tt{
		handler: func(http.ResponseWriter, *http.Request) error {
			return httperr.WithStatus(http.StatusNotFound, errors.New("missing"))
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()

		rec := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/", nil)

		if tt.ctx != nil {
			req = req.WithContext(tt.ctx)
		}
		mw := New()
		mw.SetErrorReporter(BasicReporter)
		mw.Handler(tt.handler).ServeHTTP(rec, req)

		res := rec.Result()
		defer res.Body.Close() //nolint:errcheck // we don't care

		if d := testy.DiffHTTPResponse(testy.Snapshot(t), res); d != nil {
			t.Error(d)
		}
	})
}

func TestMiddlewares(t *testing.T) {
	mws := New()

	var order []string
	mw1 := func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			order = append(order, "one")
			return next.ServeHTTPE(w, r)
		})
	}
	mw2 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			order = append(order, "two")
			next.ServeHTTP(w, r)
		})
	}
	mw3 := func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			order = append(order, "three")
			return next.ServeHTTPE(w, r)
		})
	}

	mws.Use(mw1)
	mws.UseStd(mw2)
	mws.Use(mw3)

	var err error
	mws.SetErrorReporter(func(_ http.ResponseWriter, _ *http.Request, e error) {
		err = e
	})

	h := mws.Handler(HandlerFunc(func(http.ResponseWriter, *http.Request) error {
		return errors.New("foo")
	}))

	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/", nil)

	h.ServeHTTP(rec, req)

	if !testy.ErrorMatches("foo", err) {
		t.Errorf("Unexpected err: %s", err)
	}
	want := []string{"one", "two", "three"}
	if d := testy.DiffInterface(want, order); d != nil {
		t.Errorf("Unexpected order: %v", d)
	}
}

func TestMiddlewares_already_written(t *testing.T) {
	mws := New()

	var order []string
	mw1 := func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			order = append(order, "one")
			return next.ServeHTTPE(w, r)
		})
	}
	mw2 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			order = append(order, "two")
			next.ServeHTTP(w, r)
		})
	}
	mw3 := func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			order = append(order, "three")
			return next.ServeHTTPE(w, r)
		})
	}

	mws.Use(mw1)
	mws.UseStd(mw2)
	mws.Use(mw3)

	var err error
	mws.SetErrorReporter(func(w http.ResponseWriter, r *http.Request, e error) {
		err = e
		BasicReporter(w, r, e)
	})

	h := mws.Handler(HandlerFunc(func(w http.ResponseWriter, _ *http.Request) error {
		w.WriteHeader(http.StatusOK)
		return errors.New("foo")
	}))

	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/", nil)

	h.ServeHTTP(rec, req)

	res := rec.Result()
	defer res.Body.Close() //nolint:errcheck // don't care

	if !testy.ErrorMatches("foo", err) {
		t.Errorf("Unexpected err: %s", err)
	}
	want := []string{"one", "two", "three"}
	if d := testy.DiffInterface(want, order); d != nil {
		t.Errorf("Unexpected order: %v", d)
	}
	if d := testy.DiffHTTPResponse(testy.Snapshot(t), res); d != nil {
		t.Error(d)
	}
}

func TestMiddlewares_already_written_logger(t *testing.T) {
	mws := New()
	mws.SetLogger(func(*LogContext) {})

	var order []string
	mw1 := func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			order = append(order, "one")
			return next.ServeHTTPE(w, r)
		})
	}
	mw2 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			order = append(order, "two")
			next.ServeHTTP(w, r)
		})
	}
	mw3 := func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			order = append(order, "three")
			return next.ServeHTTPE(w, r)
		})
	}

	mws.Use(mw1)
	mws.UseStd(mw2)
	mws.Use(mw3)

	var err error
	mws.SetErrorReporter(func(w http.ResponseWriter, r *http.Request, e error) {
		err = e
		BasicReporter(w, r, e)
	})

	h := mws.Handler(HandlerFunc(func(w http.ResponseWriter, _ *http.Request) error {
		w.WriteHeader(http.StatusOK)
		return errors.New("foo")
	}))

	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/", nil)

	h.ServeHTTP(rec, req)

	res := rec.Result()
	defer res.Body.Close() //nolint:errcheck // don't care

	if !testy.ErrorMatches("foo", err) {
		t.Errorf("Unexpected err: %s", err)
	}
	want := []string{"one", "two", "three"}
	if d := testy.DiffInterface(want, order); d != nil {
		t.Errorf("Unexpected order: %v", d)
	}
	if d := testy.DiffHTTPResponse(testy.Snapshot(t), res); d != nil {
		t.Error(d)
	}
}

func Test_mixed_middleware_chain(t *testing.T) {
	mw := New()

	mw.Use(func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			err := next.ServeHTTPE(w, r)
			return fmt.Errorf("one: %w", err)
		})
	})
	mw.Use(func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			err := next.ServeHTTPE(w, r)
			return fmt.Errorf("two: %w", err)
		})
	})
	mw.UseStd(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("X-Middleware", "standard one")
			next.ServeHTTP(w, r)
		})
	})
	mw.Use(func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			err := next.ServeHTTPE(w, r)
			return fmt.Errorf("three: %w", err)
		})
	})
	mw.UseStd(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("X-Middleware", "standard two")
			next.ServeHTTP(w, r)
		})
	})
	mw.Use(func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			err := next.ServeHTTPE(w, r)
			return fmt.Errorf("four: %w", err)
		})
	})

	handler := HandlerFunc(func(http.ResponseWriter, *http.Request) error {
		return errors.New("orig")
	})

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	mw.Handler(handler).ServeHTTP(rec, req)

	res := rec.Result()
	defer res.Body.Close()

	if d := testy.DiffHTTPResponse(testy.Snapshot(t), res); d != nil {
		t.Error(d)
	}
}

func TestErrorReporter(t *testing.T) {
	t.Run("multiple calls", func(t *testing.T) {
		count := 0
		reporter := ErrorReporter(func(http.ResponseWriter, *http.Request, error) {
			count++
		})
		mw := NewRouter()
		mw.ErrorReporter(reporter)
		mw.Use(reporter.Middleware)
		mw.Use(StdMiddleware(func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				next.ServeHTTP(w, r)
			})
		}))
		mw.Use(reporter.Middleware)
		mw.Get("/", HandlerFunc(func(http.ResponseWriter, *http.Request) error {
			return errors.New("foo")
		}))

		rec := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		mw.ServeHTTP(rec, req)

		if count != 1 {
			t.Errorf("error reporter called %d times, should be once", count)
		}

		res := rec.Result()
		defer res.Body.Close()

		if d := testy.DiffHTTPResponse(testy.Snapshot(t), res); d != nil {
			t.Error(d)
		}
	})
}
