package middleware

import (
	"net/http"

	"gitlab.com/flimzy/ale"
)

// LogCallback is a function to long a request.
type LogCallback func(*ale.LogContext)

// RequestStats gathers request statistics for logging, and calls cb to do
// the actual logging.
//
// Deprecated: Use ale.StdRequestLogger
func RequestStats(cb LogCallback) func(http.Handler) http.Handler {
	return ale.StdRequestLogger(cb)
}
