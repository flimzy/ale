package ale

import (
	"context"
	"errors"
	"io"
	"net/http"
	"sync/atomic"
)

type readerCounter struct {
	io.ReadCloser
	bytes int64
}

func (r *readerCounter) Read(p []byte) (int, error) {
	n, err := r.ReadCloser.Read(p)
	atomic.AddInt64(&r.bytes, int64(n))
	return n, err
}

var logDataKey = &contextKey{"logData"}

// LogData adds key/value to the context, to be included in the LogContext
// by the RequestLogger middleware. If called outside of the RequestLogger
// chain, it will panic.
func LogData(ctx context.Context, key string, value interface{}) {
	data, ok := ctx.Value(logDataKey).(*map[string]interface{})
	if !ok {
		panic("LogData must be called with a http.Request context in a RequestLogger chain")
	}
	(*data)[key] = value
}

// RequestLogger gathers request statistics for logging, and calls cb to do the
// actual logging. Any error will be included in the logging context, and
// consumed. This handler always returns a nil error.
func RequestLogger(cb func(*LogContext)) func(Handler) Handler {
	return func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			logCtx := NewLogContext(w, r)
			ctx := r.Context()
			ctx = context.WithValue(ctx, logDataKey, &logCtx.Data)
			counter := &readerCounter{ReadCloser: r.Body}
			r.Body = counter
			err := next.ServeHTTPE(logCtx, r.WithContext(ctx))
			repErr := reportedError{}
			if errors.As(err, &repErr) {
				err = repErr.error
			}
			logCtx.Finalize()
			logCtx.RequestBytes = counter.bytes
			logCtx.Error = err
			cb(logCtx)
			return nil
		})
	}
}

// StdRequestLogger gathers request statistics for logging, and calls cb to do
// the actual logging. If HandleErrors was called first, any error will be
// included in the logging context.
func StdRequestLogger(cb func(*LogContext)) func(http.Handler) http.Handler {
	return ConvertMiddleware(RequestLogger(cb))
}
