// Package errors is a drop-in replacement for the errors package in the
// standard library, with extensions useful for developing web applications.
package errors

import (
	"fmt"
	"io"
)

// Reimplemented from pkg/errors:

// New returns an error with the supplied message.
// New also records the stack trace at the point it was called.
func New(message string) error {
	return &fundamental{
		msg:   message,
		stack: callers(0),
	}
}

// fundamental is an error that has a message and a stack, but no caller.
type fundamental struct {
	msg string
	*stack
}

func (f *fundamental) Error() string { return f.msg }

func (f *fundamental) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			_, _ = io.WriteString(s, f.msg)
			f.stack.Format(s, verb)
			return
		}
		fallthrough
	case 's':
		_, _ = io.WriteString(s, f.msg)
	case 'q':
		_, _ = fmt.Fprintf(s, "%q", f.msg)
	}
}

type withStack struct {
	error
	*stack
}

func (w *withStack) Cause() error { return w.error }

// Unwrap provides compatibility for Go 1.13 error chains.
func (w *withStack) Unwrap() error { return w.error }

func (w *withStack) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			fmt.Fprintf(s, "%+v", w.Cause())
			w.stack.Format(s, verb)
			return
		}
		fallthrough
	case 's':
		_, _ = io.WriteString(s, w.Error())
	case 'q':
		_, _ = fmt.Fprintf(s, "%q", w.Error())
	}
}

// Errorf formats according to a format specifier and returns the string as a
// value that satisfies error.
//
// If the format specifier includes a %w verb with an error operand, the
// returned error will implement an Unwrap method returning the operand. It is
// invalid to include more than one %w verb or to supply it with an operand that
// does not implement the error interface. The %w verb is otherwise a synonym for %v.
//
// Errorf also records the stack trace at the point it was called.
func Errorf(format string, args ...interface{}) error {
	return &withStack{
		fmt.Errorf(format, args...),
		callers(0),
	}
}

// WithStack annotates err with a stack trace at the point WithStack was
// called. If err is nil, WithStack returns nil.
func WithStack(err error) error {
	return WithStackN(err, 1) // 1 to skip the WithStack frame
}

// AttachStack annotates err with the provided stack trace.
func AttachStack(err error, st StackTrace) error {
	frames := make(stack, len(st))
	for i, f := range st {
		frames[i] = uintptr(f)
	}
	return &withStack{
		err,
		&frames,
	}
}

// WithStackN annotates err with a stack trace at the point WithStackN was
// called, minus skip frames. If err is nil, WithStack returns nil.
func WithStackN(err error, skip int) error {
	if err == nil {
		return nil
	}
	return &withStack{
		err,
		callers(skip),
	}
}
