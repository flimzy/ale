package errors

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/flimzy/testy"
)

func Test_multi_Error(t *testing.T) {
	t.Run("zero", func(t *testing.T) {
		m := multi{}
		want := ""
		if got := m.Error(); got != want {
			t.Errorf("Unexpected result: %s", got)
		}
	})
	t.Run("one", func(t *testing.T) {
		m := multi{errors.New("foo")}
		want := "foo"
		if got := m.Error(); got != want {
			t.Errorf("Unexpected result: %s", got)
		}
	})
	t.Run("three", func(t *testing.T) {
		m := multi{errors.New("one"), errors.New("two"), errors.New("three")}
		want := "one\ntwo\nthree"
		if got := m.Error(); got != want {
			t.Errorf("Unexpected result: %s", got)
		}
	})
}

func TestAppend(t *testing.T) {
	type tt struct {
		err  error
		errs []error
		want error
	}

	tests := testy.NewTable()
	tests.Add("all nil input", tt{
		err:  nil,
		errs: nil,
		want: nil,
	})
	tests.Add("single input", tt{
		err:  errors.New("1"),
		errs: nil,
		want: errors.New("1"),
	})
	tests.Add("single append", tt{
		err:  nil,
		errs: []error{errors.New("1")},
		want: errors.New("1"),
	})
	tests.Add("two", tt{
		err:  errors.New("1"),
		errs: []error{errors.New("2")},
		want: multi{errors.New("1"), errors.New("2")},
	})
	tests.Add("append to multi", tt{
		err:  multi{errors.New("1")},
		errs: []error{errors.New("2"), errors.New("3")},
		want: multi{errors.New("1"), errors.New("2"), errors.New("3")},
	})
	tests.Add("faltten multi error", tt{
		err:  multi{errors.New("1")},
		errs: nil,
		want: errors.New("1"),
	})
	tests.Add("empty multi", tt{
		err:  multi{},
		errs: nil,
		want: nil,
	})
	tests.Add("empty append", tt{
		err:  nil,
		errs: multi{},
		want: nil,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		got := Append(tt.err, tt.errs...)
		if d := testy.DiffInterface(tt.want, got); d != nil {
			t.Error(d)
		}
	})
}

func TestInspectErrors(t *testing.T) {
	type tt struct {
		err error
	}
	tests := testy.NewTable()

	tests.Add("nil", tt{error(nil)})
	tests.Add("standard", tt{errors.New("standard")})
	tests.Add("multierr", tt{multi{
		errors.New("qwe"),
		errors.New("rty"),
	}})
	tests.Add("wrapped", tt{
		fmt.Errorf("wrapper: %w", multi{
			errors.New("uio"),
			errors.New("pas"),
		}),
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		got := InspectErrors(tt.err)
		if d := testy.DiffInterface(testy.Snapshot(t), got); d != nil {
			t.Error(d)
		}
	})
}

func TestReadChanN(t *testing.T) {
	// This test ensures that we read only 2 values from the channel, even
	// though three are sent.
	errCh := make(chan error, 3)
	go func() {
		for _, str := range []string{"foo", "bar", "baz"} {
			errCh <- errors.New(str)
		}
	}()
	got := ReadChanN(errCh, 2)
	if d := testy.DiffInterface(testy.Snapshot(t), got); d != nil {
		t.Error(d)
	}
}
