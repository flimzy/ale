package errors

import "strings"

type multi []error

func (m multi) Error() string {
	errText := make([]string, len(m))
	for i, err := range m {
		errText[i] = err.Error()
	}
	return strings.Join(errText, "\n")
}

func (m multi) Unwrap() error {
	if len(m) == 0 {
		return nil
	}
	return m[0]
}

func (m multi) Errors() []error {
	return m
}

// Append joins zero or more errors into a single error. Any nil values are
// excluded, returning nil if all inputs are nil. Any multi-errors in the input
// are treated as multiple errors. If the final result is a single error, it is
// returned unaltered, otherwise a multi-error value is returned, which can in
// turn be exploded by calling the Errors() method.
func Append(err error, errs ...error) error {
	result := make(multi, 0, len(errs)+1) // Preallocate should handle common cases
	result = append(result, explode(err)...)
	result = append(result, explode(errs...)...)

	switch len(result) {
	case 0:
		return nil
	case 1:
		return result[0]
	}
	return result
}

type multiErr interface {
	Errors() []error
}

func explode(errs ...error) []error {
	if len(errs) == 0 {
		return nil
	}
	result := make([]error, 0, len(errs))
	for _, err := range errs {
		if err == nil {
			continue
		}
		if merr, ok := err.(multiErr); ok {
			result = append(result, merr.Errors()...)
			continue
		}
		result = append(result, err)
	}
	return result
}

// ReadChan reads errors from an error channel, until the channel is closed,
// returning a multi-error. To inspect the individual errors, use the Errors()
// method, or use the InspectErrors function.
func ReadChan(errCh <-chan error) error {
	errs := []error{}
	for err := range errCh {
		errs = append(errs, err)
	}
	return Append(nil, errs...)
}

// ReadChanN reads up to n errors from an error channel, returning a
// multi-error.  To inspect the individual errors, use the Errors() method, or
// use the InspectErrors function.
//
// When n values (including nils) are read, or errCh is closed,
// the function returns. If n=0, there is no limit.
func ReadChanN(errCh <-chan error, n int) error {
	if n == 0 {
		return ReadChan(errCh)
	}
	errs := make([]error, 0, n)
	for err := range errCh {
		errs = append(errs, err)
		if len(errs) == cap(errs) {
			break
		}
	}
	return Append(nil, errs...)
}

// InspectErrors explodes a multi-error, such as that returned by Append, and
// returns a slice of the underlying errors. If err is not a multi-error, it
// will be the only element in the returned slice.
//
// Any error which satisfies the following method is considered a multi-error:
//
//	type multiError interface {
//		Errors() error
//	}
func InspectErrors(err error) []error {
	if err == nil {
		return nil
	}
	var merr interface{ Errors() []error }
	if As(err, &merr) {
		return merr.Errors()
	}
	return []error{err}
}
