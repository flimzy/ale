package errors

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ExtractGRPCStatus works just like the standard status.FromError(), except
// that it works on wrapped errors as well. The bool value will be true if err
// wraps a GRPCStatus error. If err is not a GRPCStatus error, a Status with
// code.Unknown is returned. If err is nil, a status with code OK and message
// OK is returned.
func ExtractGRPCStatus(err error) (*status.Status, bool) {
	if err == nil {
		return status.New(codes.OK, "OK"), false
	}
	var ge interface {
		GRPCStatus() *status.Status
	}
	if As(err, &ge) {
		return ge.GRPCStatus(), true
	}
	return status.New(codes.Unknown, err.Error()), false
}

// InspectGRPCStatus is like ExtractGRPCStatus, but disregards the bool value.
func InspectGRPCStatus(err error) *status.Status {
	status, _ := ExtractGRPCStatus(err)
	return status
}

// ExtractGRPCCode works just like ExtractGRPCCode, but returns only the
// status code.
func ExtractGRPCCode(err error) (codes.Code, bool) {
	status, ok := ExtractGRPCStatus(err)
	return status.Code(), ok
}

// InspectGRPCCode works just like InspectGRPCStatus, but returns only the
// status code.
func InspectGRPCCode(err error) codes.Code {
	return InspectGRPCStatus(err).Code()
}
