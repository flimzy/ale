package errors

import (
	"fmt"
	"net/http"
	"time"
)

type withRetryAfter struct {
	error
	retry string
}

func (e *withRetryAfter) Cause() error       { return e.error }
func (e *withRetryAfter) Unwrap() error      { return e.error }
func (e *withRetryAfter) RetryAfter() string { return e.retry }

// WithoutRetryAfter will wrap err with an error that returns an empty string
// for a RetryAfter query.
func WithoutRetryAfter(err error) error {
	return &withRetryAfter{
		error: err,
		retry: "",
	}
}

// WithRetryAfter wraps err such that when queried with InspectRetryAfter, it
// will return a RetryAfter value based on t.
func WithRetryAfter(err error, t time.Time) error {
	return &withRetryAfter{
		error: err,
		retry: t.UTC().Format(http.TimeFormat),
	}
}

// WithRetryAfterDur wraps err such that when queried with InspectRetryAfter,
// it will return a RetryAfter header based on dur.
func WithRetryAfterDur(err error, dur time.Duration) error {
	return &withRetryAfter{
		error: err,
		retry: fmt.Sprintf("%.0f", dur.Seconds()),
	}
}

type retryAfterErr interface {
	RetryAfter() string
}

// InspectRetryAfter returns the outter-most RetryAfter value in the error
// stack.
//
// If err is nil, there is no RetryAfter value found, or the RetryAfter value
// is blank (as when set by WithoutRetryAfter), the empty string is returned.
func InspectRetryAfter(err error) string {
	if err == nil {
		return ""
	}
	var rae retryAfterErr
	if As(err, &rae) {
		return rae.RetryAfter()
	}
	return ""
}
