package errors

import (
	"fmt"
	"runtime"

	pkgerrors "github.com/pkg/errors"
)

// Frame represents a program counter inside a stack frame.
// For historical reasons if Frame is interpreted as a uintptr
// its value represents the program counter + 1.
type Frame = pkgerrors.Frame

// StackTrace is stack of Frames from innermost (newest) to outermost (oldest).
type StackTrace = pkgerrors.StackTrace

// Callers generates a StackTrace. If n > 0, n frames are skipped.
func Callers(n int) StackTrace {
	return callers(n).StackTrace()
}

// stack represents a stack of program counters.
type stack []uintptr

func (s *stack) Format(st fmt.State, verb rune) {
	switch verb {
	case 'v':
		switch {
		case st.Flag('+'):
			for _, pc := range *s {
				f := Frame(pc)
				fmt.Fprintf(st, "\n%+v", f)
			}
		}
	}
}

func (s *stack) StackTrace() StackTrace {
	f := make([]Frame, len(*s))
	for i := 0; i < len(f); i++ {
		f[i] = Frame((*s)[i])
	}
	return f
}

func callers(skip int) *stack {
	const depth = 32
	var pcs [depth]uintptr
	n := runtime.Callers(3+skip, pcs[:])
	var st stack = pcs[0:n]
	return &st
}

// InspectStackTrace returns the first (outter-most) stacktrace in err, or nil
// if none is found.
func InspectStackTrace(err error) StackTrace {
	var seErr stackTracer
	if As(err, &seErr) {
		return seErr.StackTrace()
	}
	return nil
}

type stackTracer interface {
	error
	StackTrace() StackTrace
}

// InspectDeepStackTrace returns the last (inner-most) stacktrace in error, or
// nil if none is found
func InspectDeepStackTrace(err error) StackTrace {
	var seErr stackTracer
	var last stackTracer
	for ; err != nil; err = Unwrap(err) {
		if As(err, &seErr) {
			last = seErr
			err = seErr
		}
	}
	if last != nil {
		return last.StackTrace()
	}
	return nil
}
