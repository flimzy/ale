package errors

import (
	"errors"
	"testing"

	"gitlab.com/flimzy/testy"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestInspectGRPCStatus(t *testing.T) {
	type tt struct {
		err  error
		want string
		code codes.Code
	}

	tests := testy.NewTable()
	tests.Add("nil", tt{
		err:  nil,
		want: "OK",
		code: codes.OK,
	})
	tests.Add("non-gRPC error", tt{
		err:  errors.New("foo"),
		want: "foo",
		code: codes.Unknown,
	})
	tests.Add("gRPC error", tt{
		err:  status.Error(codes.NotFound, "missing"),
		want: "missing",
		code: codes.NotFound,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		status := InspectGRPCStatus(tt.err)
		got := status.Message()
		code := status.Code()
		if got != tt.want || code != tt.code {
			t.Errorf("Unexpected result: %v %v", code, got)
		}
	})
}

func TestInspectGRPCCode(t *testing.T) {
	type tt struct {
		err  error
		code codes.Code
	}

	tests := testy.NewTable()
	tests.Add("nil", tt{
		err:  nil,
		code: codes.OK,
	})
	tests.Add("non-gRPC error", tt{
		err:  errors.New("foo"),
		code: codes.Unknown,
	})
	tests.Add("gRPC error", tt{
		err:  status.Error(codes.NotFound, "missing"),
		code: codes.NotFound,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		code := InspectGRPCCode(tt.err)
		if code != tt.code {
			t.Errorf("Unexpected result: %v", code)
		}
	})
}
