package errors

import (
	"errors"
	stderr "errors"
	"net/http"
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestInspectFields(t *testing.T) {
	type tt struct {
		err  error
		want map[string]interface{}
	}

	tests := testy.NewTable()
	tests.Add("nil", tt{
		err:  nil,
		want: nil,
	})
	tests.Add("no fields", tt{
		err:  stderr.New("foo"),
		want: nil,
	})
	tests.Add("one field", tt{
		err:  WithField(stderr.New("foo"), "foo", "bar"),
		want: map[string]interface{}{"foo": "bar"},
	})
	tests.Add("wrapped", tt{
		err: WithField(WithField(stderr.New("foo"), "bar", "baz"), "foo", "bar"),
		want: map[string]interface{}{
			"foo": "bar",
			"bar": "baz",
		},
	})
	tests.Add("wrapped fields", tt{
		err: WithField(WithFields(stderr.New("foo"), Fields{"bar": "baz"}), "foo", "bar"),
		want: map[string]interface{}{
			"foo": "bar",
			"bar": "baz",
		},
	})
	tests.Add("duplicate key", tt{
		err: WithField(WithField(stderr.New("foo"), "foo", "baz"), "foo", "bar"),
		want: map[string]interface{}{
			"foo": "bar",
		},
	})
	tests.Add("notes field", tt{
		err: NewNotes().
			Field("foo", "bar").
			Wrap(errors.New("foo")),
		want: map[string]interface{}{
			"foo": "bar",
		},
	})
	tests.Add("notes fields", tt{
		err: NewNotes().
			Fields(Fields{"foo": "bar", "bar": "baz"}).
			Field("bar", "moo").
			Wrap(errors.New("foo")),
		want: map[string]interface{}{
			"foo": "bar",
			"bar": "moo",
		},
	})
	tests.Add("http status wrapper", tt{
		err: WithHTTPStatus(WithField(WithStack(
			WithField(errors.New("foo"), "id", "123"),
		), "asdf", "foo"), http.StatusNotFound),
		want: map[string]interface{}{
			"asdf": "foo",
			"id":   "123",
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		got := InspectFields(tt.err)
		if d := testy.DiffInterface(tt.want, got); d != nil {
			t.Error(d)
		}
	})
}
