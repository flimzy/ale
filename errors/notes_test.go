package errors

import (
	"errors"
	"fmt"
	"regexp"
	"testing"
)

func TestNotes_Wrap(t *testing.T) {
	t.Run("nil", func(t *testing.T) {
		got := NewNotes().Wrap(nil)
		if got != nil {
			t.Error("expected nil")
		}
	})
	t.Run("no fields", func(t *testing.T) {
		got := InspectFields(NewNotes().Wrap(errors.New("foo")))
		if got != nil {
			t.Error("expected nil fields")
		}
	})
	t.Run("no stack", func(t *testing.T) {
		got := InspectStackTrace(NewNotes().NoStack().Wrap(errors.New("foo")))
		if got != nil {
			t.Errorf("wanted nil stack")
		}
	})
	t.Run("wrapped no stack", func(t *testing.T) {
		err := New("foo")
		err = NewNotes().NoStack().Wrap(err)
		stack := InspectStackTrace(err)
		want := regexp.MustCompile(`^\[\]errors\.Frame{notes_test.go:30,`)
		if got := fmt.Sprintf("%#v", stack); !want.MatchString(got) {
			t.Errorf("Unexpected output: %s", got)
		}
	})
	t.Run("StackN", func(t *testing.T) {
		err := New("foo")
		err = NewNotes().StackN(2).Wrap(err)
		stack := InspectStackTrace(err)
		want := regexp.MustCompile(`^\[\]errors\.Frame{asm_amd64.s`)
		if got := fmt.Sprintf("%#v", stack); !want.MatchString(got) {
			t.Errorf("Unexpected output: %s", got)
		}
	})
	t.Run("Errorf", func(t *testing.T) {
		err := New("foo")
		err = NewNotes().StackN(2).Errorf("wrapped: %w", err)
		stack := InspectStackTrace(err)
		want := regexp.MustCompile(`^\[\]errors\.Frame{asm_amd64.s`)
		if got := fmt.Sprintf("%#v", stack); !want.MatchString(got) {
			t.Errorf("Unexpected output: %s", got)
		}
	})
}
