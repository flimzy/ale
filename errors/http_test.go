package errors

import (
	stderr "errors"
	"net/http"
	"testing"

	echo3 "github.com/labstack/echo"
	echo4 "github.com/labstack/echo/v4"
	"gitlab.com/flimzy/testy"
)

func TestInspectHTTPStatus(t *testing.T) {
	type tt struct {
		err  error
		want int
	}

	tests := testy.NewTable()
	tests.Add("nil", tt{
		err:  nil,
		want: 0,
	})
	tests.Add("normal error", tt{
		err:  New("foo"),
		want: http.StatusInternalServerError,
	})
	tests.Add("standard http code error", tt{
		err:  StatusBadRequest,
		want: http.StatusBadRequest,
	})
	tests.Add("echo 4 error", tt{
		err:  echo4.NewHTTPError(http.StatusNotFound, "missing"),
		want: http.StatusNotFound,
	})
	tests.Add("echo 3 error", tt{
		err:  echo3.NewHTTPError(http.StatusNotFound, "missing"),
		want: http.StatusNotFound,
	})
	tests.Add("notes", tt{
		err:  NewNotes().HTTPStatus(http.StatusNotFound).Wrap(stderr.New("foo")),
		want: http.StatusNotFound,
	})
	tests.Add("wrapped notes", tt{
		err:  WithHTTPStatus(NewNotes().HTTPStatus(http.StatusNotFound).Wrap(stderr.New("foo")), http.StatusBadRequest),
		want: http.StatusBadRequest,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		if got := InspectHTTPStatus(tt.err); got != tt.want {
			t.Errorf("Want: %d, Got: %d", tt.want, got)
		}
	})
}
