package view

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/flimzy/ale/httperr"
)

// ErrorHandler is the default error handler. It logs the error to stderr, for
// debugging, and returns a generic error to the HTTP client, to prevent leaking
// of any sensitive information that may be contained in unvetted errors.
//
// If err implements the StatusError interface, the returned status code will
// be sent to the client. Otherwise, status 500 (Internal Server Error) will
// be used.
var ErrorHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	err := ReqGetError(r)
	fmt.Fprintf(os.Stderr, "[ErrorHandler] %v\n", err)
	status := httperr.HTTPStatus(err)
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(status)
	if _, err := fmt.Fprintf(w, "%d %s", status, http.StatusText(status)); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to write HTTP response: %s\n", err)
	}
})
