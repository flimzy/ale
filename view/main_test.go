package view

import (
	"io"
	"net/http"
	"net/http/httptest"
)

func newRequest(method, path string, body io.Reader) *http.Request {
	return initData(httptest.NewRequest(method, path, body))
}
