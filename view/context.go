package view

import (
	"context"
	"net/http"
)

type contextKey = struct {
	name string
}

var (
	errorKey          = &contextKey{"error"}
	dataKey           = &contextKey{"data"}
	renderingErrorKey = &contextKey{"renderingError"}
)

func reqSetError(r *http.Request, err error) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), errorKey, err))
}

// GetError returns the template rendering error stored in the context.
func GetError(ctx context.Context) error {
	v, _ := ctx.Value(errorKey).(error)
	return v
}

// ReqGetError returns the template rendering error stored in the request's
// context.
func ReqGetError(r *http.Request) error {
	return GetError(r.Context())
}

func initData(r *http.Request) *http.Request {
	if _, ok := r.Context().Value(dataKey).(map[string]interface{}); ok {
		return r
	}
	data := map[string]interface{}{
		"_request": r,
	}
	return r.WithContext(context.WithValue(r.Context(), dataKey, data))
}

// GetData returns the data structure which will be passed to the template for
// rendering.
func GetData(ctx context.Context) map[string]interface{} {
	return ctx.Value(dataKey).(map[string]interface{})
}

// ReqGetData returns the data structure which will be passed to the template for
// rendering.
func ReqGetData(r *http.Request) map[string]interface{} {
	return GetData(r.Context())
}

func setRenderingError(r *http.Request) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), renderingErrorKey, true))
}

func getRenderingError(r *http.Request) bool {
	v, _ := r.Context().Value(renderingErrorKey).(bool)
	return v
}
