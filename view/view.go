package view

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/oxtoacart/bpool"

	"gitlab.com/flimzy/ale"
	"gitlab.com/flimzy/ale/httperr"
)

// Template wraps html/template.Template
type Template struct {
	rootPath     string
	include      []string
	searchPath   []string
	extension    string
	errorHandler http.Handler
	dirIndex     []string
	entryPoint   string
	bpool        *bpool.BufferPool
}

// Config are the configuration options for a Template.
type Config struct {
	// RootPath, if provided, will be stripped from the beginning of
	// request URLs before traversing SearchPath.
	//
	// Any requests not under RootPath will return a 500 error.
	RootPath string

	// Include is a list of file globs to be loaded with each template.
	Include []string

	// SearchPath is a list of directories which are to be recursively traversed to
	// look for templates to parse.
	SearchPath []string

	// TemplateExtension is the extensions which will be considered for
	// parsing when traversing IncludePaths. If unset, all files will be
	// considered.
	TemplateExtension string

	// EntryPoint is the name of the template to execute instead of the
	// default. Typically, this would be a template loaded from the Helpers
	// list.
	EntryPoint string

	// ErrorHandler will receive any unhandled errors for processing. If
	// not defined, the default is used.
	ErrorHandler http.Handler

	// DirectoryIndex sets the list of resources to look for when a client
	// requests a directory. Do not include TemplateExtension in files in
	// this list.
	DirectoryIndex []string
}

// New instantiates a new Template, based on Config.
func New(conf Config) (*Template, error) {
	var extension string
	if conf.TemplateExtension != "" {
		extension = "." + strings.TrimPrefix(conf.TemplateExtension, ".")
	}
	t := &Template{
		rootPath:     "/" + strings.Trim(conf.RootPath, "/"),
		include:      conf.Include,
		searchPath:   conf.SearchPath,
		extension:    extension,
		errorHandler: conf.ErrorHandler,
		dirIndex:     conf.DirectoryIndex,
		entryPoint:   conf.EntryPoint,
		bpool:        bpool.NewBufferPool(64),
	}
	if t.errorHandler == nil {
		t.errorHandler = ErrorHandler
	}
	return t, nil
}

// Render renders the template for r.
func (t *Template) Render(w http.ResponseWriter, r *http.Request) {
	if err := t.render(w, r); err != nil {
		t.RenderError(w, r, err)
	}
}

func (t *Template) render(w http.ResponseWriter, r *http.Request) error {
	path, err := t.resolvePath(r)
	if err != nil {
		return err
	}
	tmpl := template.New("")
	for _, include := range t.include {
		if _, err := tmpl.ParseGlob(include); err != nil {
			return err
		}
	}
	if _, err = tmpl.ParseFiles(path); err != nil {
		return err
	}
	data := ReqGetData(r)
	entryPoint := filepath.Base(path)
	if t.entryPoint != "" {
		entryPoint = t.entryPoint
	}
	buf := t.bpool.Get()
	defer t.bpool.Put(buf)
	if err := tmpl.ExecuteTemplate(buf, entryPoint, data); err != nil {
		return err
	}
	if status, ok := data["_status"].(int); ok {
		w.WriteHeader(status)
	}
	if _, err := buf.WriteTo(w); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to write response to client: %s", err)
	}
	return nil
}

// resolvePath attempts to determine which template should be rendered
// for the request.
func (t *Template) resolvePath(r *http.Request) (string, error) {
	reqPath, ok := ReqGetData(r)["_template"].(string)
	if ok {
		reqPath = strings.TrimSuffix(strings.TrimPrefix(reqPath, "/"), t.extension)
	} else {
		reqPath = r.URL.Path
		if t.rootPath != "" {
			reqPath = strings.TrimPrefix(reqPath, t.rootPath)
			if reqPath == r.URL.Path {
				// Nothing trimmed
				return "", httperr.Errorf(http.StatusInternalServerError, "Requested path '%s' not under RootPath '%s'", r.URL.Path, t.rootPath)
			}
		}
	}
	fullPath, err := t.search(reqPath)
	if err != nil || fullPath != "" {
		return fullPath, err
	}
	for _, idx := range t.dirIndex {
		fullPath, err := t.search(filepath.Join(reqPath, idx))
		if err != nil || fullPath != "" {
			return fullPath, err
		}
	}
	return "", httperr.Standard(http.StatusNotFound)
}

func (t *Template) search(reqPath string) (string, error) {
	for _, path := range t.searchPath {
		fullPath := filepath.Join(path, reqPath)
		if t.extension != "" {
			fullPath = fullPath + t.extension
		}
		f, err := os.Stat(fullPath)
		if err == nil {
			if f.IsDir() {
				return "", httperr.Errorf(http.StatusInternalServerError, "'%s' is a dir, expected a template", fullPath)
			}
			return fullPath, nil
		}
		if !os.IsNotExist(err) {
			return "", httperr.WithStack(http.StatusInternalServerError, err)
		}
	}
	return "", nil
}

// RenderError is called to render any errors.
func (t *Template) RenderError(w http.ResponseWriter, r *http.Request, err error) {
	if getRenderingError(r) {
		fmt.Fprintf(os.Stderr, "Loop detected rendering error. Falling back to default error handler.\nRequest path: %s\nError: %s\n", r.URL.Path, err)
		ErrorHandler(w, r)
		return
	}
	r = reqSetError(r, err)
	r = setRenderingError(r)
	t.errorHandler.ServeHTTP(w, r)
}

// Middleware allows you to use a Template instance as middleware, passing
// control on to other standard http handlers (or middleware).
//
// When using this method, the view will only be rendered if next doesn't write
// to w. If next does write to n, this method acts essentially as a pass-through.
func (t *Template) Middleware(next http.Handler) http.Handler {
	return ale.StdTrackWriterUsage(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = initData(r)
		next.ServeHTTP(w, r)
		if used, _ := ale.IsUsed(w); used {
			return
		}
		t.Render(w, r)
	}))
}

// ErrorHandlerFunc extends a standard http.HandlerFunc
type ErrorHandlerFunc func(http.ResponseWriter, *http.Request) error

// Handle wraps an ErrorHandlerFunc, returning a standard http.Handler.
func (t *Template) Handle(fn ErrorHandlerFunc) http.Handler {
	return t.Middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := fn(w, r); err != nil {
			t.RenderError(w, r, err)
		}
	}))
}

// HandleFunc wraps an ErrorHandlerFunc, returning a standard http.HandlerFunc
func (t *Template) HandleFunc(fn ErrorHandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		t.Handle(fn).ServeHTTP(w, r)
	}
}

// NotFoundHandler is an http.HandlerFunc which can be passed to your
// router to handle not-found errors. It calls RenderError{} with
// httperr.StandardError(http.StatusNotFound) as the error.
func (t *Template) NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	t.RenderError(w, r, httperr.Standard(http.StatusNotFound))
}
