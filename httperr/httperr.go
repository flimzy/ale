// Package httperr provides HTTP-centric extensions to standard errors.
package httperr

import (
	"fmt"
	"net/http"

	"github.com/pkg/errors"
)

type statusCoder interface {
	Error() string
	StatusCode() int
}

type httpStatuser interface {
	Error() string
	HTTPStatus() int
}

// StatusCode is an alias for HTTPStatus.
//
// Deprecated: Use HTTPStatus.
func StatusCode(err error) int {
	return HTTPStatus(err)
}

// HTTPStatus attempts to extract the HTTP status code from err, falling back
// to http.StatusInternalServerError if none is found.
func HTTPStatus(err error) int {
	var status int
	var sc statusCoder
	var hs httpStatuser
	if errors.As(err, &sc) {
		status = sc.StatusCode()
	} else if errors.As(err, &hs) {
		status = hs.HTTPStatus()
	}
	if status == 0 {
		status = http.StatusInternalServerError
	}
	return status
}

type wrappedError struct {
	err    error
	status int
}

var _ statusCoder = &wrappedError{}

func (e *wrappedError) Error() string {
	return e.err.Error()
}

func (e *wrappedError) HTTPStatus() int {
	return e.status
}

func (e *wrappedError) StatusCode() int {
	return e.status
}

func (e *wrappedError) Cause() error {
	return e.err
}

// Standard converts a standard HTTP status code into a useable error
type Standard int

var _ statusCoder = Standard(0)

func (e Standard) Error() string {
	if str := http.StatusText(int(e)); str != "" {
		return str
	}
	return fmt.Sprintf("HTTP Status %d", int(e))
}

// StatusCode returns e.
func (e Standard) StatusCode() int {
	return int(e)
}

// HTTPStatus returns e.
func (e Standard) HTTPStatus() int {
	return int(e)
}

// New wraps pkg/errors.New()
func New(status int, msg string) error {
	return &wrappedError{
		err:    errors.New(msg),
		status: status,
	}
}

// Errorf wraps pkg/errors.Errorf()
func Errorf(status int, format string, args ...interface{}) error {
	return &wrappedError{
		err:    errors.Errorf(format, args...),
		status: status,
	}
}

// Wrap wraps pkg/errors.Wrap()
func Wrap(status int, err error, msg string) error {
	if err == nil {
		return nil
	}
	return &wrappedError{
		err:    errors.Wrap(err, msg),
		status: status,
	}
}

// WithStack wraps pkg/errors.WithStack()
func WithStack(status int, err error) error {
	if err == nil {
		return nil
	}
	return &wrappedError{
		err:    errors.WithStack(err),
		status: status,
	}
}

// Wrapf wraps pkg/errors.Wrapf()
func Wrapf(status int, err error, format string, args ...interface{}) error {
	if err == nil {
		return nil
	}
	return &wrappedError{
		err:    errors.Wrapf(err, format, args...),
		status: status,
	}
}

// WithStatus attaches a status code to an existing error.
func WithStatus(status int, err error) error {
	if err == nil {
		return nil
	}
	return &wrappedError{
		err:    err,
		status: status,
	}
}

type buriedError struct {
	msg    string
	err    error
	status int
}

var _ statusCoder = &buriedError{}

func (e *buriedError) Error() string {
	return e.msg
}

func (e *buriedError) StatusCode() int {
	return e.status
}

func (e *buriedError) HTTPStatus() int {
	return e.status
}

func (e *buriedError) Cause() error {
	return e.err
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func (e *buriedError) StatckTrace() errors.StackTrace {
	return e.err.(stackTracer).StackTrace()
}

// Bury buries err, exposing only msg to the end user. err is still accessible
// through Cause()
func Bury(status int, err error, msg string) error {
	if err == nil {
		return nil
	}
	return &buriedError{
		msg:    msg,
		err:    errors.WithStack(err),
		status: status,
	}
}

// Buryf works like Bury.
func Buryf(status int, err error, format string, args ...interface{}) error {
	if err == nil {
		return nil
	}
	return Bury(status, err, fmt.Sprintf(format, args...))
}
