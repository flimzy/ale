package httperr

import (
	"errors"
	"net/http"
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestStatusCode(t *testing.T) {
	type tst struct {
		err      error
		expected int
	}
	tests := testy.NewTable()
	tests.Add("standard", tst{
		err:      errors.New("foo"),
		expected: http.StatusInternalServerError,
	})
	tests.Add("wrappedError", tst{
		err: &wrappedError{
			err:    errors.New("foo"),
			status: http.StatusNotFound,
		},
		expected: http.StatusNotFound,
	})
	tests.Add("StandardError", tst{
		err:      Standard(http.StatusNotFound),
		expected: http.StatusNotFound,
	})

	tests.Run(t, func(t *testing.T, test tst) {
		code := StatusCode(test.err)
		if code != test.expected {
			t.Errorf("Unexpected status. Expected %d, got %d", test.expected, code)
		}
	})
}
