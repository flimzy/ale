package ale

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"gitlab.com/flimzy/testy"
)

func TestStdRequestLogger(t *testing.T) {
	var result *LogContext
	cb := func(c *LogContext) {
		result = &LogContext{
			Request:         c.Request,
			StatusCode:      c.StatusCode,
			ResponseHeader:  c.ResponseHeader,
			ResponseTrailer: c.ResponseTrailer,
			ResponseBytes:   c.ResponseBytes,
			RequestBytes:    c.RequestBytes,
			Data:            c.Data,
		}
	}
	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			t.Fatal(err)
		}
		w.Header().Set("Trailer:Foo", "123")
		w.Header().Set("Bar", "abc")
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("foo"))
		LogData(r.Context(), "extra", "data")
	})
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, "/", strings.NewReader("foobar"))
	StdRequestLogger(cb)(next).ServeHTTP(w, r)

	result.StartTime = time.Time{}
	result.ElapsedTime = 0
	expected := &LogContext{
		Request:         r,
		StatusCode:      200,
		ResponseHeader:  http.Header{"Bar": []string{"abc"}},
		ResponseBytes:   3,
		RequestBytes:    6,
		ResponseTrailer: http.Header{"Foo": []string{"123"}},
		Data: map[string]interface{}{
			"extra": "data",
		},
	}
	if d := testy.DiffInterface(expected, result); d != nil {
		t.Error(d)
	}
}
