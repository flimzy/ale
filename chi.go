package ale

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

/*
	This file wraps and adapts the chi router
*/

// Router is copied from chi, but adapted for the ale.Handler type.
type Router interface {
	http.Handler
	Handler

	// EarlyUse registers middlewares in the call chain before the error
	// reporter or logger. It is intended for setting metadata, such as trace
	// IDs, that are necessary to properly handle errors or log requests.
	//
	// Calling EarlyUse on a sub-router will panic.
	EarlyUse(middlewares ...Middleware)
	// ErrorReporter defines an error reporter at the top of the call chain.
	// The error reporter should turn an error value into an HTTP response to
	// be consumed by the client.
	//
	// It is not uncommon to also want to set an error handler further down the
	// call chain, to consume additional information (such as authenticated user
	// trace IDs, etc) that may be added to the context later in the middleware
	// call chain. It is therefore safe to use ErrorReporter and also call
	// Use(ale.ErrorReporter(<your func>).Middleware).  The ErrorReporter type
	// ensures that any given error is not reported more than once.
	ErrorReporter(ErrorReporter)
	Logger(func(*LogContext))

	// Use appends one or more middlewares onto the Router stack.
	Use(middlewares ...Middleware)

	// With adds inline middlewares for an endpoint handler.
	With(middlewares ...Middleware) Router

	// Group adds a new inline-Router along the current routing
	// path, with a fresh middleware stack for the inline-Router.
	Group() Router

	// Route mounts a sub-Router along a `pattern“ string.
	Route(pattern string) Router

	// Mount attaches another Handler along ./pattern/*
	Mount(pattern string, h Handler)

	// Handle and HandleFunc adds routes for `pattern` that matches
	// all HTTP methods.
	Handle(pattern string, h Handler)
	HandleFunc(pattern string, h HandlerFunc)

	// Method and MethodFunc adds routes for `pattern` that matches
	// the `method` HTTP method.
	Method(method, pattern string, h Handler)
	MethodFunc(method, pattern string, h HandlerFunc)

	// HTTP-method routing along `pattern`
	Connect(pattern string, h HandlerFunc)
	Delete(pattern string, h HandlerFunc)
	Get(pattern string, h HandlerFunc)
	Head(pattern string, h HandlerFunc)
	Options(pattern string, h HandlerFunc)
	Patch(pattern string, h HandlerFunc)
	Post(pattern string, h HandlerFunc)
	Put(pattern string, h HandlerFunc)
	Trace(pattern string, h HandlerFunc)

	// NotFound defines a handler to respond whenever a route could
	// not be found.
	NotFound(h HandlerFunc)

	// MethodNotAllowed defines a handler to respond whenever a method is
	// not allowed.
	MethodNotAllowed(h HandlerFunc)
}

// NewRouter returns a new router (based on the chi router).
//
// Experimental
func NewRouter() Router {
	return &mux{
		Mux: chi.NewRouter(),
	}
}

type mux struct {
	earlyMiddlewares []Middleware
	middlewares      []Middleware
	*chi.Mux
	inline   bool
	handler  Handler
	reporter ErrorReporter
	logger   func(*LogContext)
}

var _ Router = &mux{}

func (mx *mux) Logger(fn func(*LogContext)) {
	mx.logger = fn
}

func (mx *mux) ErrorReporter(reporter ErrorReporter) {
	mx.reporter = reporter
}

func (mx *mux) EarlyUse(middlewares ...Middleware) {
	if mx.inline {
		panic("EarlyUse may only be called on the top-level router")
	}
	mx.earlyMiddlewares = append(mx.earlyMiddlewares, middlewares...)
}

func (mx *mux) Use(middlewares ...Middleware) {
	mx.middlewares = append(mx.middlewares, middlewares...)
}

func (mx *mux) updateRouteHandler() {
	mx.handler = chain(mx.middlewares, Wrap(mx.Mux))
}

func chain(middlewares []Middleware, endpoint Handler) Handler {
	if len(middlewares) == 0 {
		return endpoint
	}

	h := middlewares[len(middlewares)-1](endpoint)
	for i := len(middlewares) - 2; i >= 0; i-- {
		h = middlewares[i](h)
	}

	return h
}

func (mx *mux) With(middlewares ...Middleware) Router {
	// Similarly as in handle(), we must build the mux handler once additional
	// middleware registration isn't allowed for this stack, like now.
	if !mx.inline && mx.handler == nil {
		mx.updateRouteHandler()
	}

	// Copy middlewares from parent inline muxs
	var mws []Middleware
	if mx.inline {
		mws = make([]Middleware, len(mx.middlewares))
		copy(mws, mx.middlewares)
	}
	mws = append(mws, middlewares...)

	return &mux{
		inline:      true,
		middlewares: mws,
		Mux:         mx.Mux.With().(*chi.Mux),
	}
}

func (mx *mux) Group() Router {
	return mx.With()
}

func (m *mux) Route(pattern string) Router {
	subRouter := NewRouter()
	m.Mount(pattern, subRouter)
	return subRouter
}

func (m *mux) Mount(pattern string, h Handler) {
	m.Mux.Mount(pattern, Std(h))
}

func (mx *mux) httpHandler(handler Handler) http.Handler {
	var h Handler
	if mx.inline {
		h = chain(mx.middlewares, handler)
	} else {
		h = handler
	}
	return Std(h)
}

func (mx *mux) Handle(pattern string, h Handler) {
	mx.Mux.Handle(pattern, mx.httpHandler(h))
}

func (mx *mux) HandleFunc(pattern string, h HandlerFunc) {
	mx.Mux.HandleFunc(pattern, mx.httpHandler(h).ServeHTTP)
}

func (mx *mux) Method(method, pattern string, h Handler) {
	mx.Mux.Method(method, pattern, mx.httpHandler(h))
}

func (mx *mux) MethodFunc(method, pattern string, h HandlerFunc) {
	mx.Method(method, pattern, h)
}

func (mx *mux) Connect(pattern string, h HandlerFunc) {
	mx.Method(http.MethodConnect, pattern, h)
}

func (m *mux) Delete(pattern string, h HandlerFunc) {
	m.Method(http.MethodDelete, pattern, h)
}

func (m *mux) Get(pattern string, h HandlerFunc) {
	m.Method(http.MethodGet, pattern, h)
}

func (m *mux) Head(pattern string, h HandlerFunc) {
	m.Method(http.MethodHead, pattern, h)
}

func (m *mux) Options(pattern string, h HandlerFunc) {
	m.Method(http.MethodOptions, pattern, h)
}

func (m *mux) Patch(pattern string, h HandlerFunc) {
	m.Method(http.MethodPatch, pattern, h)
}

func (m *mux) Post(pattern string, h HandlerFunc) {
	m.Method(http.MethodPost, pattern, h)
}

func (m *mux) Put(pattern string, h HandlerFunc) {
	m.Method(http.MethodPut, pattern, h)
}

func (m *mux) Trace(pattern string, h HandlerFunc) {
	m.Method(http.MethodTrace, pattern, h)
}

func (m *mux) NotFound(h HandlerFunc) {
	m.Mux.NotFound(Std(h).ServeHTTP)
}

func (m *mux) MethodNotAllowed(h HandlerFunc) {
	m.Mux.MethodNotAllowed(Std(h).ServeHTTP)
}

func (mx *mux) ServeHTTPE(w http.ResponseWriter, r *http.Request) error {
	if mx.handler == nil {
		mx.updateRouteHandler()
	}
	h := mx.handler
	if mx.reporter != nil {
		h = mx.reporter.Middleware(h)
	}
	h = TrackWriterUsage(h)
	if mx.logger != nil {
		h = RequestLogger(mx.logger)(h)
	}
	h = chain(mx.earlyMiddlewares, h)
	err := h.ServeHTTPE(w, r)
	if _, ok := err.(reportedError); ok {
		return nil
	}
	return err
}

func (m *mux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	HandlerFunc(m.ServeHTTPE).ServeHTTP(w, r)
}
